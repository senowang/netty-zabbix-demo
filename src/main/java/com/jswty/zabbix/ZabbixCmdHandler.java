package com.jswty.zabbix;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;

/**
 * @author senowang
 * @date 2018/10/31 - 15:47
 */
public class ZabbixCmdHandler implements Runnable {
    private String cmd;
    private ChannelHandlerContext ctx;
    
    public ZabbixCmdHandler(String cmd, ChannelHandlerContext ctx) {
        this.cmd = cmd;
        this.ctx = ctx;
    }
    
    @Override
    public void run() {
        //dosomething....
        String result = "";
        if (cmd != null) {
        
        }else {
            result = "unsupport key";
        }
        ActiveMessage activeMessage = new ActiveMessage();
        activeMessage.bulidMessage(result);
        ByteBuf messageByteBuf = activeMessage.getMessageByteBuf();
        ctx.write(messageByteBuf);
    }
}
