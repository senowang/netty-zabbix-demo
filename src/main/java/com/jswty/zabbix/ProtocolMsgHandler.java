package com.jswty.zabbix;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

import java.io.UnsupportedEncodingException;

/**
 * @author senowang
 * @date 2018/10/31 - 16:32
 */
public class ProtocolMsgHandler {
    protected static ByteBuf getMessageByteBuf(String message) {
        byte[] messageBytes = new byte[0];
        try {
            messageBytes = message.getBytes("utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        int length = messageBytes.length;
        ByteBuf byteBuf = Unpooled.buffer(length + 13);
        byte[] bytes = new byte[]{'Z', 'B', 'X', 'D', '\1', (byte) (length & 0xFF), (byte) ((length >> 8) & 0x00FF), (byte) ((length >> 16) & 0x0000FF), (byte) ((length >> 24) & 0x000000FF), '\0', '\0', '\0', '\0'};
        byteBuf.writeBytes(bytes);
        byteBuf.writeBytes(messageBytes);
        return byteBuf;
    }
}
