package com.jswty.zabbix;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

import java.io.UnsupportedEncodingException;

/**
 * @author senowang
 * @date 2018/10/31 - 16:18
 */
public class PassvieMessage {
    private String message;
    
    PassvieMessage(String message) {
        this.message = message;
    }
    
    private String getMessage() {
        return message;
    }
    
    public ByteBuf getMessageByteBuf() {
        return ProtocolMsgHandler.getMessageByteBuf(message);
    }
    
}
