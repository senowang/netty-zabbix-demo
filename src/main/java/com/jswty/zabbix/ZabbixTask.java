package com.jswty.zabbix;

import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

/**
 * @author senowang
 * @date 2018/10/29 - 17:35
 */
public class ZabbixTask implements Delayed {
    
    private ZabbixItem zabbixItem;
    private long delay;
    private long expire;
    
    public ZabbixItem getZabbixItem() {
        return zabbixItem;
    }
  
    public ZabbixTask(ZabbixItem zabbixItem,long delay) {
        this.zabbixItem = zabbixItem;
        this.delay=delay;
        expire=System.currentTimeMillis()+delay*1000L;
    }
    
    @Override
    public int compareTo(Delayed delayed) {
        ZabbixTask task = (ZabbixTask) delayed;
        int i = (int) (this.getDelay(TimeUnit.MILLISECONDS) - task.getDelay(TimeUnit.MILLISECONDS));
        return i;
    }
    
    @Override
    public long getDelay(TimeUnit unit) {
        long convert = unit.convert(this.expire - System.currentTimeMillis(), TimeUnit.MILLISECONDS);

        return convert;
    }
}
