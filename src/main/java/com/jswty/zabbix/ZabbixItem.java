package com.jswty.zabbix;

/**
 * @author senowang
 * @date 2018/10/29 - 17:38
 */
public class ZabbixItem {
    /**
     * 主机名
     */
    private String hostname;
    /**
     * 监控项key
     */
    private String key;
    /**
     * 监控项原始key
     */
    private String key_orig;
    /**
     * 监控项采集频率
     */
    private int delay;
    /**
     * 监控项为日志类型的最后采集日志大小
     */
    private long lastlogsize;
    /**
     * 监控项为日志采集最后采集时间
     */
    private long mtime;
    
    public String getHostname() {
        return hostname;
    }
    
    public void setHostname(String hostname) {
        this.hostname = hostname;
    }
    
    public String getKey() {
        return key;
    }
    
    public void setKey(String key) {
        this.key = key;
    }
    
    public String getKey_orig() {
        return key_orig;
    }
    
    public void setKey_orig(String key_orig) {
        this.key_orig = key_orig;
    }
    
    public int getDelay() {
        return delay;
    }
    
    public void setDelay(int delay) {
        this.delay = delay;
    }
    
    public long getLastlogsize() {
        return lastlogsize;
    }
    
    public void setLastlogsize(long lastlogsize) {
        this.lastlogsize = lastlogsize;
    }

    public long getMtime() {
        return mtime;
    }

    public void setMtime(long mtime) {
        this.mtime = mtime;
    }
    
    @Override
    public String toString() {
        return "ZabbixItem{" + "hostname='" + hostname + '\'' + ", key='" + key + '\'' + ", key_orig='" + key_orig + '\'' + ", delay=" + delay + ", lastlogsize=" + lastlogsize + ", mtime=" + mtime + '}';
    }
}
