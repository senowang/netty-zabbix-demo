package com.jswty;

import com.jswty.zabbix.ActiveMessage;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;

/**
 * @author senowang
 * @date 2018/10/15 - 16:15
 */
public class ZabbixSender {
    
    public static final Integer PORT = 10055;
    private static final String IP = "127.0.0.1";
    
    private static void sendData(String message) {
        OutputStream out = null;
        InputStream inputStream = null;
        Socket socket = connect();
        try {
            out = socket.getOutputStream();
            inputStream = socket.getInputStream();
            wirteMessage(out, encodeString(message));
            out.flush();
            String responeMessage = getResponeMessage(inputStream, socket);
            System.out.println(responeMessage);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                    inputStream.close();
                }
            } catch (IOException e) {
            }
            try {
                if (socket != null) {
                    socket.close();
                }
            } catch (IOException e) {
            }
        }
    }
    
    private static Socket connect() {
        SocketAddress address = new InetSocketAddress(IP, PORT);
        Socket socket = new Socket();
        try {
            socket.connect(address);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return socket;
    }
    
    public static void wirteMessage(OutputStream out, byte[] message) throws IOException {
        int length = message.length;
        out.write(new byte[]{'Z', 'B', 'X', 'D', '\1', (byte) (length & 0xFF), (byte) ((length >> 8) & 0x00FF), (byte) ((length >> 16) & 0x0000FF), (byte) ((length >> 24) & 0x000000FF), '\0', '\0', '\0', '\0'});
        out.write(message);
    }
    
    public static byte[] encodeString(String data) {
        try {
            if (data != null) {
                return data.getBytes("UTF-8");
            } else {
                return "".getBytes("UTF-8");
            }
        } catch (UnsupportedEncodingException e) {
            return data.getBytes();
        }
    }
    private static String getResponeMessage(InputStream inputStream, Socket socket) throws IOException {
        byte[] head = new byte[5];
        byte[] length = new byte[8];
        byte[] content = null;
        if (socket.isConnected()) {
            inputStream.read(head);
            inputStream.read(length);
            int targets = byte2int(length);
            content = new byte[targets];
            inputStream.read(content);
            return new String(content);
        } else {
            return null;
        }
    }
    private static int byte2int(byte[] bytes) {
        return (bytes[0] & 0xff) | ((bytes[1] << 8) & 0xff00) | (bytes[2] << 16) & 0xFF0000 | (bytes[3] << 24) & 0xFF000000;
    }
    
    public static void main(String[] args) {
       
        ActiveMessage activeMessage =new ActiveMessage();
        String  map= activeMessage.bulidMessage("test");
       
        sendData(map);
    }
}
