package com.jswty;

import com.jswty.zabbix.ZabbixCmdHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author senowang
 * @date 2018/10/15 - 15:26
 */
public class ZabbixAgentPassiveHandler extends ChannelInboundHandlerAdapter {
    
    private ExecutorService executorService = Executors.newFixedThreadPool(5, Executors.privilegedThreadFactory());
    /**
     * Creates a client-side handler.
     */
    public ZabbixAgentPassiveHandler() {
    
    }
    
    @Override
    public void channelActive(ChannelHandlerContext ctx) {
    
    }
    
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {
        String name = ctx.name();
        System.out.println("ctx-name:"+name);
        String cmd=(String) msg;
        System.out.println("agent revicer server message:"+msg);
        executorService.submit(new ZabbixCmdHandler(cmd,ctx));
    }
    
    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) {
        ctx.flush();
    }
    
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        // Close the connection when an exception is raised.
        cause.printStackTrace();
        ctx.close();
    }
}
