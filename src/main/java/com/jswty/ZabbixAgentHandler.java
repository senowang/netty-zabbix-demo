package com.jswty;

import com.jswty.zabbix.ActiveMessage;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

/**
 * @author senowang
 * @date 2018/10/15 - 15:26
 */
public class ZabbixAgentHandler extends ChannelInboundHandlerAdapter {
    
    
    /**
     * Creates a client-side handler.
     */
    public ZabbixAgentHandler() {
    
    }
    
    @Override
    public void channelActive(ChannelHandlerContext ctx) {
        ActiveMessage activeMessage = new ActiveMessage("test");
        ctx.writeAndFlush(activeMessage.getMessageByteBuf());
    }
    
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {
        String message = (String) msg;
        System.out.println(message);
        ctx.close();
    }
    
    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) {
        ctx.flush();
    }
    
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        // Close the connection when an exception is raised.
        cause.printStackTrace();
        ctx.close();
    }
}
