package zabbix;

import com.jswty.zabbix.ZabbixItem;
import com.jswty.zabbix.ZabbixTask;

import java.util.concurrent.DelayQueue;

/**
 * @author senowang
 * @date 2018/10/29 - 17:52
 */
public class DelayedTest {
    static DelayQueue<ZabbixTask> delayQueue = new DelayQueue<ZabbixTask>();
    
    public static void main(String[] args) {
        init();
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    ZabbixTask take = null;
                    try {
                        take = delayQueue.take();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
            
                    }
                    ZabbixItem zabbixItem = take.getZabbixItem();
//                    System.out.println(Thread.currentThread().getName()+"-getDelay\t"+convert);
                    System.out.println(System.currentTimeMillis() + "\t" + zabbixItem.getKey());
                    delayQueue.add(new ZabbixTask(zabbixItem, zabbixItem.getDelay()));
                }
            }
        }).start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    ZabbixTask take = null;
                    try {
                        take = delayQueue.take();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    
                    }
                    ZabbixItem zabbixItem = take.getZabbixItem();
                    System.out.println(System.currentTimeMillis() + "\t" + zabbixItem.getKey());
                    delayQueue.add(new ZabbixTask(zabbixItem, zabbixItem.getDelay()));
                }
            }
        }).start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    ZabbixTask take = null;
                    try {
                        take = delayQueue.take();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    
                    }
                    ZabbixItem zabbixItem = take.getZabbixItem();
                    System.out.println(System.currentTimeMillis() + "\t" + zabbixItem.getKey());
                    delayQueue.add(new ZabbixTask(zabbixItem, zabbixItem.getDelay()));
                }
            }
        }).start();
        
    }
    
    
    public static void init() {
        for (int i = 0; i < 100000; i++) {
            ZabbixItem zabbixItem = new ZabbixItem();
            zabbixItem.setDelay(10);
            zabbixItem.setHostname("test_10");
            zabbixItem.setKey("key_10");
            ZabbixTask zabbixTask = new ZabbixTask(zabbixItem, zabbixItem.getDelay());
            delayQueue.offer(zabbixTask);
            zabbixItem = new ZabbixItem();
            zabbixItem.setDelay(20);
            zabbixItem.setHostname("test_20");
            zabbixItem.setKey("key_20");
            zabbixTask = new ZabbixTask(zabbixItem, zabbixItem.getDelay());
            delayQueue.offer(zabbixTask);
            zabbixItem = new ZabbixItem();
            zabbixItem.setDelay(30);
            zabbixItem.setHostname("test_30");
            zabbixItem.setKey("key_30");
            zabbixTask = new ZabbixTask(zabbixItem, zabbixItem.getDelay());
            delayQueue.offer(zabbixTask);
        }
        
    }
}
